﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public struct Operation
    {
        public decimal FirstArgument { get; private set; }

        public decimal SecondArgument { get; private set; }

        public char TypeOperation { get; }

        public static List<string> PriorityOperations = new List<string>() { "*/", "+-" };

        public Operation(decimal firstArgument, decimal secondArgument, char typeOperation)
        {
            FirstArgument = firstArgument;
            SecondArgument = secondArgument;
            TypeOperation = typeOperation;
        }

        public decimal Calculate(out string errorMessage)
        {
            errorMessage = string.Empty;
            switch (TypeOperation)
            {
                case '+': return Addition(ref errorMessage);
                case '-': return Subtraction(ref errorMessage);
                case '*': return Multiplication(ref errorMessage);
                case '/': return Division(ref errorMessage);
                default: { errorMessage = "Неподдерживаемая операция"; return 0; }
            };
        }

        private decimal Addition(ref string errorMessage)
        {
            bool isOverflow = Math.Sign(FirstArgument) == Math.Sign(SecondArgument) && decimal.MaxValue - Math.Abs(FirstArgument) < Math.Abs(SecondArgument);
            if (isOverflow)
            {
                errorMessage = "Переполнение в результате сложения";
                return 0;
            }
            return FirstArgument + SecondArgument;
        }

        private decimal Subtraction(ref string errorMessage)
        {
            bool isOverflow = Math.Sign(FirstArgument) != Math.Sign(SecondArgument) && decimal.MaxValue - Math.Abs(FirstArgument) < Math.Abs(SecondArgument);
            if(isOverflow)
            {
                errorMessage = "Переполнение в результате вычитания";
                return 0;
            }
            return FirstArgument - SecondArgument;
        }

        private decimal Multiplication(ref string errorMessage)
        {
            if (FirstArgument != 0 && SecondArgument != 0)
            {
                bool isOverflow = false;
                var firstArgument = Math.Abs(FirstArgument);
                var secondArgument = Math.Abs(SecondArgument);
                if (firstArgument > 1)
                {
                    isOverflow = decimal.MaxValue / firstArgument < secondArgument;
                }
                else if(secondArgument > 1)
                {
                    isOverflow = decimal.MaxValue / secondArgument < firstArgument;
                }
                if (isOverflow)
                {
                    errorMessage = "Переполнение в результате умножения";
                    return 0;
                }
            }
            return FirstArgument * SecondArgument;
        }

        private decimal Division(ref string errorMessage)
        {
            if(SecondArgument == 0)
            {
                errorMessage = "Деление на ноль невозможно";
                return 0;
            }
            if(FirstArgument != 0)
            {
                bool isOverflow = false;
                var firstArgument = Math.Abs(FirstArgument);
                var secondArgument = Math.Abs(SecondArgument);
                if(firstArgument < 1)
                {
                    isOverflow = decimal.MaxValue * firstArgument < secondArgument;
                }
                else if(secondArgument < 1)
                {
                    isOverflow = decimal.MaxValue * secondArgument < firstArgument;
                }
                if(isOverflow)
                {
                    errorMessage = "Переполнение в результате деления";
                    return 0;
                }
            }
            return FirstArgument / SecondArgument;
        }
    }
}
