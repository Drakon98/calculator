﻿using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Calculator
{
    public class Parser
    {
        public NumberFormatInfo ParseNumberFormat { get; private set; }

        public Parser(NumberFormatInfo numberFormatInfo)
        {
            ParseNumberFormat = numberFormatInfo;
        }

        public string ParseExample(StringBuilder example)
        {
            example.Replace(',', '.');
            decimal result = 0;
            List<Expression> expressions = FindExpressions(example, out string errorMessage);

            while (expressions.Count > 0 && errorMessage == string.Empty)
            {
                int maxRang = expressions.Max(x => x.Rang);
                int minRang = expressions.Min(x => x.Rang);
                var expression = expressions.First(x => x.Rang == maxRang);
                var saveString = expression.Value.ToString();
                result = expression.Calculate(ref errorMessage);
                if (expressions.Count == 1) break;
                for (int i = maxRang - 1; i > minRang - 1; i--)
                {
                    var newExpression = expressions.FirstOrDefault(x => x.Rang == i && x.StartIndex <= expression.StartIndex && x.EndIndex >= expression.EndIndex && x.Value.ToString().Contains(saveString));
                    if (newExpression != null)
                    {
                        expressions[expressions.IndexOf(newExpression)].Value.Replace(saveString, result.ToString().Replace(',', ParseNumberFormat.NumberDecimalSeparator[0]));
                        expressions.Remove(expression);
                    }
                    else break;
                }
            }

            if (errorMessage == string.Empty)
                return $"{example}={result}";
            else
                return $"{example}={errorMessage}";
        }

        public List<Expression> FindExpressions(StringBuilder example, out string errorMessage)
        {
            errorMessage = string.Empty;
            var expressions = new List<Expression>();
            if(example[0] != '(' || example[^1] != ')')
            {
                example.Insert(0, '(');
                example.Append(')');
            }
            int counter = 0;
            Stack<int> startIndexes = new Stack<int>();
            for(int i = 0; i < example.Length; i++)
            {
                switch (example[i])
                {
                    case '(': { counter++; startIndexes.Push(i); } break;
                    case ')':
                        {
                            counter--;
                            int startIndex = startIndexes.Pop();
                            expressions.Add(new Expression(new StringBuilder(example.ToString().Substring(startIndex, i - startIndex + 1)), startIndex, i, counter));
                        } break;
                }
            }
            if(counter != 0)
            {
                errorMessage = "Неверно расставлены скобки";
            }
            return expressions;
        }
    }
}
