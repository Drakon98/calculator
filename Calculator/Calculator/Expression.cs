﻿using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace Calculator
{
    public class Expression
    {
        public StringBuilder Value { get; }

        public int StartIndex { get; }

        public int EndIndex { get; }

        public int Rang { get; }

        public char NumberDecimalSeparator { get; }

        public Expression(StringBuilder value, int startIndex, int endIndex, int rang, char numberDecimalSeparator = '.')
        {
            Value = value;
            StartIndex = startIndex;
            EndIndex = endIndex;
            Rang = rang;
            NumberDecimalSeparator = numberDecimalSeparator;
        }

        public decimal Calculate(ref string errorMessage)
        {
            decimal result = 0;
            List<Expression> expressions = new List<Expression>();
            Parse(expressions, ref errorMessage).BuildOperations(expressions, ref result, ref errorMessage);
            return result;
        }

        public bool IsMatch(char currentSymbol, string allowedSymbols)
        {
            bool isMatch = false;
            foreach(var symbol in allowedSymbols)
            {
                if(symbol == currentSymbol)
                {
                    isMatch = true;
                    break;
                }
            }
            return isMatch;
        }

        public Expression Parse(List<Expression> expressions, ref string errorMessage)
        {
            bool isStartDigit = false;
            int startIndex = 0;
            for (int i = 0; i < Value.Length; i++)
            {
                if (char.IsDigit(Value[i]) || Value[i] == NumberDecimalSeparator)
                {
                    if (i != 0)
                    {
                        if (!char.IsDigit(Value[i - 1]) && Value[i - 1] != NumberDecimalSeparator && !(Value[i - 1] == '-' && startIndex == i - 1)) isStartDigit = true;
                        else isStartDigit = false;
                    }
                    if (isStartDigit) startIndex = i;
                    if (i == Value.Length - 1) expressions.Add(new Expression(new StringBuilder(Value.ToString().Substring(startIndex)), startIndex, i, 0));
                    continue;
                }
                else if (IsMatch(Value[i], "()-+/*"))
                {
                    if (Value[i] == '(')
                    {
                        if (IsMatch(Value[i + 1], "-"))
                        {
                            i++;
                            startIndex = i;
                        }
                        continue;
                    }
                    if (Value[i] == ')')
                    {
                        if (char.IsDigit(Value[i - 1]))
                        {
                            expressions.Add(new Expression(new StringBuilder(Value.ToString().Substring(startIndex, i - startIndex)), startIndex, i - 1, 0));
                            continue;
                        }
                        else
                        {
                            errorMessage = "Неправильный формат строки";
                            break;
                        }
                    }
                    if(IsMatch(Value[i + 1], "+/*"))
                    {
                        errorMessage = "Неправильный формат строки";
                        break;
                    }
                    isStartDigit = false;
                    expressions.Add(new Expression(new StringBuilder(Value.ToString().Substring(startIndex, i - startIndex)), startIndex, i - 1, 0));
                    expressions.Add(new Expression(new StringBuilder($"{Value[i]}"), i, i, 0));
                    if (IsMatch(Value[i + 1], "-"))
                    {
                        i++;
                        startIndex = i;
                    }
                    continue;
                }
                else if(IsMatch(Value[i], "%^!"))
                {
                    errorMessage = "Неподдерживаемая операция";
                    break;
                }
                else if (char.IsLetter(Value[i]))
                {
                    errorMessage = "В примере не должны содержаться буквы";
                    break;
                }
            }
            return this;
        }

        public void BuildOperations(List<Expression> expressions, ref decimal result, ref string errorMessage)
        {
            if (errorMessage != string.Empty) return;
            var numberFormat = new NumberFormatInfo() { NumberDecimalSeparator = NumberDecimalSeparator.ToString() };
            for (int i = 0; i < Operation.PriorityOperations.Count; i++)
            {
                for (int j = 1; j < expressions.Count; j++)
                {
                    string currentExpression = expressions[j].Value.ToString();
                    if (expressions.Count > 1 && IsMatch(currentExpression[0], Operation.PriorityOperations[i]))
                    {
                        var firstExpression = expressions[j - 1].Value.ToString();
                        var secondExpression = expressions[j + 1].Value.ToString();
                        bool isParsedFirstArgument = decimal.TryParse(firstExpression, NumberStyles.Number, numberFormat, out decimal firstArgument);
                        bool isParsedSecondArgument = decimal.TryParse(secondExpression, NumberStyles.Number, numberFormat, out decimal secondArgument);
                        if (isParsedFirstArgument && isParsedSecondArgument)
                        {
                            var operation = new Operation(firstArgument, secondArgument, currentExpression[0]);
                            result = operation.Calculate(out errorMessage);
                            if (errorMessage != string.Empty) return;
                            var newExpression = new Expression(new StringBuilder(result.ToString()), expressions[j - 1].StartIndex, expressions[j - 1].StartIndex + result.ToString().Length, 0);
                            newExpression.Value.Replace(',', NumberDecimalSeparator);
                            expressions.RemoveRange(j - 1, 3);
                            expressions.Insert(j - 1, newExpression);
                            j--;
                        }
                        else
                        {
                            errorMessage = "Не удалось привести число к типу decimal";
                            return;
                        }
                    }
                }
                if (expressions.Count == 1)
                {
                    bool isParsedArgument = decimal.TryParse(expressions[0].Value.ToString(), NumberStyles.Number, numberFormat, out result);
                    if (!isParsedArgument)
                    {
                        errorMessage = "Не удалось привести число к типу decimal";
                        return;
                    }
                }
            }
        }
    }
}