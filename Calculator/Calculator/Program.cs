﻿using System;
using System.Text;
using System.Globalization;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var parser = new Parser(new NumberFormatInfo() { NumberDecimalSeparator = "." });
            var key = new ConsoleKeyInfo();
            while (key.Key != ConsoleKey.Escape)
            {
                Console.WriteLine("Введите пример без знака равенства и нажмите Enter:");
                string equality = parser.ParseExample(new StringBuilder(Console.ReadLine()));
                Console.WriteLine(equality);
                Console.WriteLine("Для продолжения нажмите Enter, для выхода Escape");
                key = Console.ReadKey();
            }
        }
    }
}
