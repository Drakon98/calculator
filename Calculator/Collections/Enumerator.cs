﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Collections
{
    public class Enumerator<T> : IEnumerator<T>
    {
        private readonly MyList<T> list;

        private int position;

        public T Current { get => list[position]; }

        object IEnumerator.Current { get => list[position]; }

        public Enumerator(MyList<T> list) => this.list = list;

        public void Dispose() { }

        public bool MoveNext()
        {
            if (position < list.Count - 1)
            {
                position++;
                return true;
            }
            else return false;
        }

        public void Reset()
        {
            position = -1;
        }
    }
}
