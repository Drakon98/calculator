﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Collections
{
    [Serializable]
    public class MyList<T> : IList<T>
    {
        private T[] list;

        private int currentCapacity;

        private const int defaultCapacity = 4;

        public T this[int index] { get => list[index]; set { list[index] = value; } }

        public int Count { get; private set; }

        public int Capacity { get => currentCapacity; set => SetCapacity(value); }

        public bool IsReadOnly { get; } = false;

        public MyList()
        {
            currentCapacity = defaultCapacity;
            list = new T[currentCapacity];
            Count = 0;
        }

        public MyList(int capacity)
        {
            currentCapacity = capacity;
            list = new T[currentCapacity];
            Count = 0;
        }

        public MyList(IEnumerable<T> collection)
        {
            if (collection == null) throw new ArgumentNullException();
            int count = 0;
            foreach(var item in collection) count++;
            Count = count;
            Capacity = count;
            list = new T[currentCapacity];
            count = 0;
            foreach(var item in collection)
            {
                list[count] = item;
                count++;
            }
        }

        private void SetCapacity(int newCapacity)
        {
            if (newCapacity <= Count)
            {
                bool isOverflow = int.MaxValue / Count < 2 ? true : false;
                int newLength = isOverflow ? int.MaxValue : Count * 2;
                currentCapacity = Count < defaultCapacity ? defaultCapacity : newLength;
            }
        }

        public void Add(T item)
        {
            if (Count == currentCapacity)
            {
                Capacity = Count;
                T[] newList = new T[currentCapacity];
                if(newList.Length > 0) CopyTo(newList, 0);
                list = newList;
            }
            list[Count++] = item;
        }

        public void Clear() => Array.Clear(list, 0, list.Length);

        public bool Contains(T item)
        {
            bool isContained = false;
            foreach(var element in list)
            {
                if(Equals(element, item))
                {
                    isContained = true;
                    break;
                }
            }
            return isContained;
        }

        public void CopyTo(T[] array, int arrayIndex) => Array.Copy(list, 0, array, arrayIndex, list.Length);

        public IEnumerator<T> GetEnumerator() =>  new Enumerator<T>(this);

        public int IndexOf(T item) => Array.IndexOf(list, item);

        public void Insert(int index, T item)
        {
            if (index < 0 || index > Count) throw new ArgumentOutOfRangeException();
            Add(item);
            if (index == Count) return;
            for (int i = Count - 1; i >= index; i--) list[i + 1] = list[i];
            list[index] = item;
        }

        public bool Remove(T item)
        {
            bool isExist = false;
            for(int i = 0; i < list.Length; i++)
            {
                if(Equals(list[i], item))
                {
                    isExist = true;
                    RemoveAt(i);
                    break;
                }
            }
            return isExist;
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index > list.Length) throw new ArgumentOutOfRangeException();
            for (int j = index; j < Count - 1; j++)
            {
                list[j] = list[j + 1];
            }
            Count--;
        }

        IEnumerator IEnumerable.GetEnumerator() => list.GetEnumerator();
    }
}
