﻿using System;
using System.Collections.Generic;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            var collection = new MyList<int>();
            for (int i = 0; i < 100; i++)
            {
                collection.Add(i);
            }
            OutputCollection(collection, 0, collection.Count);
            collection.Insert(20, 200);
            OutputCollection(collection, 0, collection.Count);
            collection.Insert(100, 200);
            OutputCollection(collection, 0, collection.Count);
            collection.Insert(102, 200);
            OutputCollection(collection, 0, collection.Count);
            bool isRemoved = collection.Remove(200);
            OutputCollection(collection, 0, collection.Count);
            foreach(var item in collection)
            {
                if (item == 200) Console.WriteLine(item);
            }
            Console.ReadKey();
        }

        private static void OutputCollection<T>(MyList<T> collection, int startIndex, int endIndex)
        {
            for (int i = startIndex; i < endIndex; i++)
            {
                Console.WriteLine($"i = {i} Value = {collection[i]} Capacity = {collection.Capacity}");
            }
            Console.WriteLine();
        }
    }
}
