﻿using NUnit.Framework;
using Calculator;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace NUnitTestCalculator
{
    [TestFixture]
    public class TestsParser
    {
        [Test]
        public void TestFindExpressions()
        {
            var example = new StringBuilder("5+(3-2)*(7-3*4)+((-1)*8)");
            var parser = new Parser(new NumberFormatInfo() { NumberDecimalSeparator = "." });
            List<Expression> expressions = parser.FindExpressions(example, out string errorMessage);
            List<string> values = expressions.Select(x => x.Value.ToString()).ToList();
            Assert.True(values[0] == "(3-2)" && values[1] == "(7-3*4)" && values[2] == "(-1)" && values[3] == "((-1)*8)" && values[4] == "(5+(3-2)*(7-3*4)+((-1)*8))");
        }

        [TestCase("700.625/0.5+((-1)*(245.85+(32.60*456.84-(200.489+20.874))))+(456.837+345.87)*3+(-245.09+34.89)", "-11318.3000")]
        [TestCase("-0.5*2.5+7.7*9.3/(2.1+4.5)", "9.60")]
        [TestCase("(-50*80+300/5*((-1)*3)+(100*5))", "-3680")]
        [TestCase("(7*8/2*(6*5-40))", "-280")]
        [TestCase("(7*8/2+(6*5-40))", "18")]
        [TestCase("(7*8/2+(6*5))", "58")]
        [TestCase("(-9*5+3)*9", "-378")]
        [TestCase("140/7*(25-7)-(81*3)", "117")]
        [TestCase("12*(8/4)-6", "18")]
        [TestCase("-3+40*2/8+5", "12")]
        [TestCase("-3*8fhенhg0", "В примере не должны содержаться буквы")]
        [TestCase("-3*80+", "Неправильный формат строки")]
        [TestCase("10-3/0", "Деление на ноль невозможно")]
        [TestCase("20^8", "Неподдерживаемая операция")]
        [TestCase("((6+8)", "Неверно расставлены скобки")]
        [TestCase("6+8***9", "Неправильный формат строки")]
        [TestCase("6+8*900.625.765", "Не удалось привести число к типу decimal")]
        [Test]
        public void TestParseExample(string example, string result)
        {
            var parser = new Parser(new NumberFormatInfo() { NumberDecimalSeparator = "." });
            string newResult = parser.ParseExample(new StringBuilder(example));
            newResult = newResult[(newResult.IndexOf('=') + 1)..];
            newResult = newResult.Replace(',', '.');
            Assert.False(newResult != result);
        }
    }
}
