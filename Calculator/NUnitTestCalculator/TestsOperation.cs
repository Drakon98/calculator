using NUnit.Framework;
using Calculator;
using System;

namespace NUnitTestCalculator
{
    public enum Limit { Max, Min }

    [TestFixture]
    public class TestsOperation
    {
        private Operation operation;

        
        [SetUp]
        public void Setup()
        {
        }

        [TestCase(Limit.Max, 10)]
        [TestCase(Limit.Max, 40.25)]
        [TestCase(Limit.Max, 0)]
        [TestCase(Limit.Max, -10)]
        [TestCase(Limit.Max, -40.25)]
        [TestCase(Limit.Min, 10)]
        [TestCase(Limit.Min, 40.25)]
        [TestCase(Limit.Min, 0)]
        [TestCase(Limit.Min, -10)]
        [TestCase(Limit.Min, -40.25)]
        [Test]
        public void TestAddition(Limit limit, decimal secondArgument)
        {
            decimal firstArgument = limit == Limit.Max ? decimal.MaxValue : decimal.MinValue;
            operation = new Operation(firstArgument, secondArgument, '+');
            decimal result = operation.Calculate(out string errorMessage);
            decimal newResult;
            try
            {
                newResult = checked(operation.FirstArgument + operation.SecondArgument);
                Assert.False(result != newResult);
            }
            catch (OverflowException)
            {
                Assert.False(result != 0);
            }
        }

        [TestCase(10, Limit.Max)]
        [TestCase(40.25, Limit.Max)]
        [TestCase(0, Limit.Max)]
        [TestCase(-10, Limit.Max)]
        [TestCase(-40.25, Limit.Max)]
        [TestCase(10, Limit.Min)]
        [TestCase(40.25, Limit.Min)]
        [TestCase(0, Limit.Min)]
        [TestCase(-10, Limit.Min)]
        [TestCase(-40.25, Limit.Min)]
        [Test]
        public void TestAddition(decimal firstArgument, Limit limit)
        {
            decimal secondArgument = limit == Limit.Max ? decimal.MaxValue : decimal.MinValue;
            operation = new Operation(firstArgument, secondArgument, '+');
            decimal result = operation.Calculate(out string errorMessage);
            decimal newResult;
            try
            {
                newResult = checked(operation.FirstArgument + operation.SecondArgument);
                Assert.False(result != newResult);
            }
            catch (OverflowException)
            {
                Assert.False(result != 0);
            }
        }

        [TestCase(200, 500)]
        [TestCase(250, -300)]
        [TestCase(-400, 200)]
        [TestCase(-400, 800)]
        [TestCase(0, 0)]
        [TestCase(-800, -1000)]
        [Test]
        public void TestAddition(decimal firstArgument, decimal secondArgument)
        {
            operation = new Operation(firstArgument, secondArgument, '+');
            decimal result = operation.Calculate(out string errorMessage);
            decimal newResult;
            try
            {
                newResult = checked(operation.FirstArgument + operation.SecondArgument);
                Assert.False(result != newResult);
            }
            catch (OverflowException)
            {
                Assert.False(result != 0);
            }
        }

        [TestCase(Limit.Max, 10)]
        [TestCase(Limit.Max, 40.25)]
        [TestCase(Limit.Max, 0)]
        [TestCase(Limit.Max, -10)]
        [TestCase(Limit.Max, -40.25)]
        [TestCase(Limit.Min, 10)]
        [TestCase(Limit.Min, 40.25)]
        [TestCase(Limit.Min, 0)]
        [TestCase(Limit.Min, -10)]
        [TestCase(Limit.Min, -40.25)]
        [Test]
        public void TestSubtraction(Limit limit, decimal secondArgument)
        {
            decimal firstArgument = limit == Limit.Max ? decimal.MaxValue : decimal.MinValue;
            operation = new Operation(firstArgument, secondArgument, '-');
            decimal result = operation.Calculate(out string errorMessage);
            decimal newResult;
            try
            {
                newResult = checked(operation.FirstArgument - operation.SecondArgument);
                Assert.False(result != newResult);
            }
            catch (OverflowException)
            {
                Assert.False(result != 0);
            }
        }

        [TestCase(10, Limit.Max)]
        [TestCase(40.25, Limit.Max)]
        [TestCase(0, Limit.Max)]
        [TestCase(-10, Limit.Max)]
        [TestCase(-40.25, Limit.Max)]
        [TestCase(10, Limit.Min)]
        [TestCase(40.25, Limit.Min)]
        [TestCase(0, Limit.Min)]
        [TestCase(-10, Limit.Min)]
        [TestCase(-40.25, Limit.Min)]
        [Test]
        public void TestSubtraction(decimal firstArgument, Limit limit)
        {
            decimal secondArgument = limit == Limit.Max ? decimal.MaxValue : decimal.MinValue;
            operation = new Operation(firstArgument, secondArgument, '-');
            decimal result = operation.Calculate(out string errorMessage);
            decimal newResult;
            try
            {
                newResult = checked(operation.FirstArgument - operation.SecondArgument);
                Assert.False(result != newResult);
            }
            catch (OverflowException)
            {
                Assert.False(result != 0);
            }
        }

        [TestCase(200, 500)]
        [TestCase(250, -300)]
        [TestCase(-400, 200)]
        [TestCase(-400, 800)]
        [TestCase(0, 0)]
        [TestCase(-800, -1000)]
        [Test]
        public void TestSubtraction(decimal firstArgument, decimal secondArgument)
        {
            operation = new Operation(firstArgument, secondArgument, '-');
            decimal result = operation.Calculate(out string errorMessage);
            decimal newResult;
            try
            {
                newResult = checked(operation.FirstArgument - operation.SecondArgument);
                Assert.False(result != newResult);
            }
            catch (OverflowException)
            {
                Assert.False(result != 0);
            }
        }

        [TestCase(Limit.Max, 10)]
        [TestCase(Limit.Max, 40.25)]
        [TestCase(Limit.Max, 1)]
        [TestCase(Limit.Max, 0.99)]
        [TestCase(Limit.Max, 0.5)]
        [TestCase(Limit.Max, 0.1)]
        [TestCase(Limit.Max, 0)]
        [TestCase(Limit.Max, -0.99)]
        [TestCase(Limit.Max, -0.5)]
        [TestCase(Limit.Max, -0.1)]
        [TestCase(Limit.Max, -1)]
        [TestCase(Limit.Max, -10)]
        [TestCase(Limit.Max, -40.25)]
        [TestCase(Limit.Min, 10)]
        [TestCase(Limit.Min, 40.25)]
        [TestCase(Limit.Min, 1)]
        [TestCase(Limit.Min, 0.99)]
        [TestCase(Limit.Min, 0.5)]
        [TestCase(Limit.Min, 0.1)]
        [TestCase(Limit.Min, 0)]
        [TestCase(Limit.Min, -0.99)]
        [TestCase(Limit.Min, -0.5)]
        [TestCase(Limit.Min, -0.1)]
        [TestCase(Limit.Min, -1)]
        [TestCase(Limit.Min, -10)]
        [TestCase(Limit.Min, -40.25)]
        [Test]
        public void TestMultiplication(Limit limit, decimal secondArgument)
        {
            decimal firstArgument = limit == Limit.Max ? decimal.MaxValue : decimal.MinValue;
            operation = new Operation(firstArgument, secondArgument, '*');
            decimal result = operation.Calculate(out string errorMessage);
            decimal newResult;
            try
            {
                newResult = checked(operation.FirstArgument * operation.SecondArgument);
                Assert.False(result != newResult);
            }
            catch (OverflowException)
            {
                Assert.False(result != 0);
            }
        }

        [TestCase(10, Limit.Max)]
        [TestCase(40.25, Limit.Max)]
        [TestCase(1, Limit.Max)]
        [TestCase(0.99, Limit.Max)]
        [TestCase(0.5, Limit.Max)]
        [TestCase(0.1, Limit.Max)]
        [TestCase(0, Limit.Max)]
        [TestCase(-0.99, Limit.Max)]
        [TestCase(-0.5, Limit.Max)]
        [TestCase(-0.1, Limit.Max)]
        [TestCase(-1, Limit.Max)]
        [TestCase(-10, Limit.Max)]
        [TestCase(-40.25, Limit.Max)]
        [TestCase(10, Limit.Min)]
        [TestCase(40.25, Limit.Min)]
        [TestCase(1, Limit.Min)]
        [TestCase(0.99, Limit.Min)]
        [TestCase(0.5, Limit.Min)]
        [TestCase(0.1, Limit.Min)]
        [TestCase(0, Limit.Min)]
        [TestCase(-0.99, Limit.Min)]
        [TestCase(-0.5, Limit.Min)]
        [TestCase(-0.1, Limit.Min)]
        [TestCase(-1, Limit.Min)]
        [TestCase(-10, Limit.Min)]
        [TestCase(-40.25, Limit.Min)]
        [Test]
        public void TestMultiplication(decimal firstArgument, Limit limit)
        {
            decimal secondArgument = limit == Limit.Max ? decimal.MaxValue : decimal.MinValue;
            operation = new Operation(firstArgument, secondArgument, '*');
            decimal result = operation.Calculate(out string errorMessage);
            decimal newResult;
            try
            {
                newResult = checked(operation.FirstArgument * operation.SecondArgument);
                Assert.False(result != newResult);
            }
            catch (OverflowException)
            {
                Assert.False(result != 0);
            }
        }

        [TestCase(200, 500)]
        [TestCase(250, -300)]
        [TestCase(-400, 200)]
        [TestCase(-400, 800)]
        [TestCase(-1, 200)]
        [TestCase(-0.99, 200)]
        [TestCase(-0.5, 200)]
        [TestCase(-0.1, 200)]
        [TestCase(1, 200)]
        [TestCase(0.99, 200)]
        [TestCase(0.5, 200)]
        [TestCase(0.1, 200)]
        [TestCase(200, -1)]
        [TestCase(200, -0.99)]
        [TestCase(200, -0.5)]
        [TestCase(200, -0.1)]
        [TestCase(200, 1)]
        [TestCase(200, 0.99)]
        [TestCase(200, 0.5)]
        [TestCase(200, 0.1)]
        [TestCase(0, 200)]
        [TestCase(0, 0)]
        [TestCase(200, 0)]
        [TestCase(-800, -1000)]
        [Test]
        public void TestMultiplication(decimal firstArgument, decimal secondArgument)
        {
            operation = new Operation(firstArgument, secondArgument, '*');
            decimal result = operation.Calculate(out string errorMessage);
            decimal newResult;
            try
            {
                newResult = checked(operation.FirstArgument * operation.SecondArgument);
                Assert.False(result != newResult);
            }
            catch (OverflowException)
            {
                Assert.False(result != 0);
            }
        }

        [TestCase(Limit.Max, 10)]
        [TestCase(Limit.Max, 40.25)]
        [TestCase(Limit.Max, 1)]
        [TestCase(Limit.Max, 0.99)]
        [TestCase(Limit.Max, 0.5)]
        [TestCase(Limit.Max, 0.1)]
        [TestCase(Limit.Max, 0)]
        [TestCase(Limit.Max, -0.99)]
        [TestCase(Limit.Max, -0.5)]
        [TestCase(Limit.Max, -0.1)]
        [TestCase(Limit.Max, -1)]
        [TestCase(Limit.Max, -10)]
        [TestCase(Limit.Max, -40.25)]
        [TestCase(Limit.Min, 10)]
        [TestCase(Limit.Min, 40.25)]
        [TestCase(Limit.Min, 1)]
        [TestCase(Limit.Min, 0.99)]
        [TestCase(Limit.Min, 0.5)]
        [TestCase(Limit.Min, 0.1)]
        [TestCase(Limit.Min, 0)]
        [TestCase(Limit.Min, -0.99)]
        [TestCase(Limit.Min, -0.5)]
        [TestCase(Limit.Min, -0.1)]
        [TestCase(Limit.Min, -1)]
        [TestCase(Limit.Min, -10)]
        [TestCase(Limit.Min, -40.25)]
        [Test]
        public void TestDivision(Limit limit, decimal secondArgument)
        {
            decimal firstArgument = limit == Limit.Max ? decimal.MaxValue : decimal.MinValue;
            operation = new Operation(firstArgument, secondArgument, '/');
            decimal result = operation.Calculate(out string errorMessage);
            decimal newResult;
            try
            {
                newResult = checked(operation.FirstArgument / operation.SecondArgument);
                Assert.False(result != newResult);
            }
            catch (OverflowException)
            {
                Assert.False(result != 0);
            }
            catch (DivideByZeroException)
            {
                Assert.False(result != 0);
            }
        }

        [TestCase(10, Limit.Max)]
        [TestCase(40.25, Limit.Max)]
        [TestCase(1, Limit.Max)]
        [TestCase(0.99, Limit.Max)]
        [TestCase(0.5, Limit.Max)]
        [TestCase(0.1, Limit.Max)]
        [TestCase(0, Limit.Max)]
        [TestCase(-0.99, Limit.Max)]
        [TestCase(-0.5, Limit.Max)]
        [TestCase(-0.1, Limit.Max)]
        [TestCase(-1, Limit.Max)]
        [TestCase(-10, Limit.Max)]
        [TestCase(-40.25, Limit.Max)]
        [TestCase(10, Limit.Min)]
        [TestCase(40.25, Limit.Min)]
        [TestCase(1, Limit.Min)]
        [TestCase(0.99, Limit.Min)]
        [TestCase(0.5, Limit.Min)]
        [TestCase(0.1, Limit.Min)]
        [TestCase(0, Limit.Min)]
        [TestCase(-0.99, Limit.Min)]
        [TestCase(-0.5, Limit.Min)]
        [TestCase(-0.1, Limit.Min)]
        [TestCase(-1, Limit.Min)]
        [TestCase(-10, Limit.Min)]
        [TestCase(-40.25, Limit.Min)]
        [Test]
        public void TestDivision(decimal firstArgument, Limit limit)
        {
            decimal secondArgument = limit == Limit.Max ? decimal.MaxValue : decimal.MinValue;
            operation = new Operation(firstArgument, secondArgument, '/');
            decimal result = operation.Calculate(out string errorMessage);
            decimal newResult;
            try
            {
                newResult = checked(operation.FirstArgument / operation.SecondArgument);
                Assert.False(result != newResult);
            }
            catch (OverflowException)
            {
                Assert.False(result != 0);
            }
            catch (DivideByZeroException)
            {
                Assert.False(result != 0);
            }
        }

        [TestCase(200, 500)]
        [TestCase(250, -300)]
        [TestCase(-400, 200)]
        [TestCase(-400, 800)]
        [TestCase(-1, 200)]
        [TestCase(-0.99, 200)]
        [TestCase(-0.5, 200)]
        [TestCase(-0.1, 200)]
        [TestCase(1, 200)]
        [TestCase(0.99, 200)]
        [TestCase(0.5, 200)]
        [TestCase(0.1, 200)]
        [TestCase(200, -1)]
        [TestCase(200, -0.99)]
        [TestCase(200, -0.5)]
        [TestCase(200, -0.1)]
        [TestCase(200, 1)]
        [TestCase(200, 0.99)]
        [TestCase(200, 0.5)]
        [TestCase(200, 0.1)]
        [TestCase(0, 200)]
        [TestCase(0, 0)]
        [TestCase(200, 0)]
        [TestCase(-800, -1000)]
        [Test]
        public void TestDivision(decimal firstArgument, decimal secondArgument)
        {
            operation = new Operation(firstArgument, secondArgument, '/');
            decimal result = operation.Calculate(out string errorMessage);
            decimal newResult;
            try
            {
                newResult = checked(operation.FirstArgument / operation.SecondArgument);
                Assert.False(result != newResult);
            }
            catch (OverflowException)
            {
                Assert.False(result != 0);
            }
            catch (DivideByZeroException)
            {
                Assert.False(result != 0);
            }
        }
    }
}