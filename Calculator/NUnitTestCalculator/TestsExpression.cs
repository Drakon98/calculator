﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Text;
using Calculator;

namespace NUnitTestCalculator
{
    [TestFixture]
    public class TestsExpression
    {
        [TestCase("(-100*8/40+67)", "-100", "*", "8", "/", "40", "+", "67")]
        [TestCase("(70-300*0.5)", "70", "-", "300", "*", "0.5")]
        [TestCase("(-3*5+3)", "-3", "*", "5", "+", "3")]
        [TestCase("(3*5+3)", "3", "*", "5", "+", "3")]
        [Test]
        public void TestParseExpression(string example, params string[] args)
        {
            var expression = new Expression(new StringBuilder(example), 0, example.Length, 0);
            bool isAssert = true;
            string errorMessage = string.Empty;
            List<Expression> expressions = new List<Expression>();
            expression.Parse(expressions, ref errorMessage);
            for(int i = 0; i < args.Length; i++)
            {
                if (expressions[i].Value.ToString() == args[i]) continue;
                else isAssert = false;
            }
            Assert.True(isAssert && expressions.Count == args.Length);
        }

        [TestCase("(-100*8/40+67)", 47)]
        [TestCase("(70-300*0.5)", -80)]
        [TestCase("(-3*5+3)", -12)]
        [TestCase("(3*5+3)", 18)]
        [Test]
        public void TestBuildOperations(string example, decimal result)
        {
            string errorMessage = string.Empty;
            decimal newResult = 0;
            var expression = new Expression(new StringBuilder(example), 0, example.Length, 0);
            List<Expression> expressions = new List<Expression>();
            expression.Parse(expressions, ref errorMessage).BuildOperations(expressions, ref newResult, ref errorMessage);
            Assert.False(result != newResult);
        }
    }
}
